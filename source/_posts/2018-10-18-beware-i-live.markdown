---
layout: post
title: "Beware, I Live!"
date: 2018-10-18 09:27:48 +0300
comments: false
categories: [About, Game Development]
---

<h3>Greetings Citizen, Welcome to my Dev Blog!</h3>

I go by the bloody magnificent (and much better than your) name of Stab Calloway.
I'm a hobbyist game developer, bad musician and terrible person of unknown age and origin.

I'll be utilizing this particular section of the interwebz to ramble about some of my latest <strike>abominations</strike> creations, hoping that publicly recording my progress
might give me the motivation to keep iterating upon the potentially useful/promising ones.
Most content will be GameDev related, but I'll probably also post some privacy/security resources
and audio production stuff.

<h3>Unity Engine Game Development</h3>
In 2014 I started programming in C# and tinkering with Unity.
As soon as Unity 5 was released in early 2015, I jumped straight in and started developing "Clarity" (working title), 
a single player first-person procedural death labyrinth with randomly generated dungeons, 
stealth mechanics, melee/ranged combat, various weapons, enemy cultists that conjure terrifying demons, 
boss battles, random loot, random potions with random side effects, armour system, weapon/item crafting, carry/weight system, modern graphics and plenty of other cool stuff.

I also have a few smaller projects that I work on whenever I get the chance.

Thanks for reading my first post, kind citizen!

[GitLab.com]: https://about.gitlab.com/gitlab-com
[pages]: https://pages.gitlab.io
